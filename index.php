<html lang="en">

<?php

	include 'include/phpFunctions.php';

	$view = FALSE;
	$add = FALSE;
	$edit = FALSE;
	$delete = FALSE;
	$viewStatus = FALSE;
	$welcome = TRUE;

	$data = new Admin;

	$alert = '';
	$status = '';
	$statusTxt = '';
	$query = '';


#RESET DATABSE

    if(isset($_POST['resetDatabase'])) {
		buildHead('Reset Database');
		buildNavBar($dataLst);

	    require_once('../../mysqli/mysqli_connect.php');

   		 foreach($data->drop as $query) {

       		 @mysqli_query($db_connect, $query);
     	 }

        	foreach($data->create as $query) {
           		 @mysqli_query($db_connect, $query);
     	  }

		$query = '';

		foreach($data->drop as $drop) {
			$query = $query . $drop . '<br/><br/>';
		}
		foreach($data->create as $create) {
			$query = $query . $create . '</br><br/>';

		}# . ' ' . $data->create;	
		$viewStatus = TRUE;		
		$alert = 'success';
		$status = 'Success';
		$statusTxt = 'ALL TABLES DROPPED AND RE-CREATED';
		$welcome = FALSE;
		$delete = TRUE;
    }


if(!$delete) {

	foreach ($dataLst as $page => $data) {
		


#VIEW RENDER

		if(isset($_POST['viewRender' . $page])) {
			buildHead($data->pageTitle);
			buildNavBar($dataLst);
			$welcome = FALSE;
			$view = TRUE;	
			break;
		}

#ADD RENDER

		if(isset($_POST['addRender' . $page])) {
			buildHead($data->pageTitle);
			buildNavBar($dataLst);
			$welcome = FALSE;
			$add = TRUE;	
			break;
		}

#EDIT RENDER

		if(isset($_POST['editRender' . $page])) {
			buildHead($data->pageTitle);
			buildNavBar($dataLst);
			$welcome = FALSE;
			$edit = TRUE;	
			break;
		}

#EDIT

		if(isset($_POST['edit' . $page])) {
			buildHead($data->pageTitle);
			buildNavBar($dataLst);

			$table = $data->table;
			$query = "UPDATE " . $table . " SET ";
			$values = "";
			$where = "";

			$first = True;	
			foreach($data->allFields as $field) {

				if($first) { #$field[4] == -1) {
					$first = False;
					$where = $where . ' WHERE ' . $field[0] . ' = ' . $_POST[$field[0]];
				}

                if($field[0] == "`LastModified`") {
					$values = $values . $field[0] . "=now(), ";
				}

				if($field[4] == 1) {
					if(empty($_POST[ $field[0] ])) {
						$data_missing[] = $field[1];
					} else {
						$values = $values . $field[0] . "=" . formatData($_POST[ $field[0] ], $field[2]) . ', ';
					}
				}

				if($field[4] == 4) {
					$values = $values . $field[0] . "=" . formatData($_POST[ $field[0] ], $field[2]) . ', ';
				}


				if($field[4] = 0)  {
					$values = $values . $field[0] . "=" . $_POST[$field[0]] . ", ";

				}
	
			}
		

			$values = substr($values, 0, -2);
			$query  = $query . $values . $where;

			require_once('../../mysqli/mysqli_connect.php');

			$db_connect->query($query);

			if($db_connect->affected_rows == 1) {

				$viewStatus = TRUE;		
				$alert = 'success';
				$status = 'Success';
				$statusTxt = 'Record updated';

			 } else {

				$viewStatus = TRUE;		
				$alert = 'danger';
				$status = 'Failure';
				$statusTxt = 'Parameters incorrect';
			}	

			$view = TRUE;

			mysqli_close($db_connect);
			$welcome = FALSE;
			break;
		} 



#ADD

		if(isset($_POST['submit'. $page])) {
			buildHead($data->pageTitle);
			buildNavBar($dataLst);
			$data_missing = array();
            $query = 'INSERT INTO ' . $data->table . '(';
            $values = 'VALUES(';

            foreach($data->allFields as $field) {
                if($field[4] == -1) {
                    $query = $query . $field[0] . ', ';
                    $values = $values . 'null, ';
                }

                if($field[4] == 0) {
                    $query = $query . $field[0] . ', ';
                    $values = $values . 'now(), ';
                }

                if($field[4] == 1) {
                    if(empty($_POST[ $field[0] ])) {
                        $data_missing[] = $field[1];
                    } else {
                        $query = $query . $field[0] . ', ';
                        $values = $values . formatData($_POST[ $field[0] ], $field[2]) . ', ';
                    }
                }

                if($field[4] == 4) {
                    $query = $query . $field[0] . ', ';
                    $values = $values . formatData($_POST[ $field[0] ], $field[2]) . ', ';
                }
            }

            $query = substr($query, 0, -2) . ') ' . substr($values, 0, -2) . ')';

            require_once('../../mysqli/mysqli_connect.php');
	
			$db_connect->query($query);
          
			if(empty($data_missing)) {

				if($db_connect->affected_rows == 1) {
					$alert = 'success';
					$status = 'Success';
					$statusTxt = 'Record added';
				
				} else {
					$alert = 'danger';
					$status = 'Error';
					$statusTxt = 'Parameters incorrect';

				}	

			} else {
			
				$alert = 'danger';
				$status = 'Error';
				$statusTxt = 'Not all required entered';
			}

			$viewStatus = TRUE;		
			
			$view = TRUE;
			$welcome = FALSE;
			break;

		}


#DELETE

		if(isset($_POST['delete' . $page  ])) {
			buildHead($data->pageTitle);
			buildNavBar($dataLst);

    		$where = $_POST['where'];
        	$dbTable = $_POST['dbTable'];

        	$query = "DELETE FROM " . $dbTable . ' ' . $where;
        	
			require_once('../../mysqli/mysqli_connect.php');

        	$response = @mysqli_query($db_connect, $query);
		
			$viewStatus = TRUE;		
			$alert = 'success';
			$status = 'Success';
			$statusTxt = 'Record deleted';

			$view = TRUE;
			$welcome = FALSE;
			break;
		}
	}

if($welcome) {
			buildHead($data->pageTitle);
			buildNavBar($dataLst);
			$viewStatus = TRUE;		
			$alert = 'info';
			$status = 'Welcome';
			$statusTxt = 'Welcome the the South West Seven Radio Management Site';

}

}
?>



<body>
	<div class="row">
		<div class="col-sm-1 container">
			<form action="" method="POST">
                <input class="btn btn-danger btn-sm" name="resetDatabase" type="submit" value="Reset Database"></input>
            </form>

		</div>

		<div class="col-sm-9">



<?php	


$statusDiv = '<div class="alert alert-' . $alert . ' fade in">
              	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>' . $status . ':</strong>  ' . $statusTxt . '</div>
			<div><small><i>' . $query . '</i></small></div>';



#RENDER VIEW
	if($view) {
                require_once('../../mysqli/mysqli_connect.php');


   #            $query = selectAll($data->allFields, $data->table);
                $query = $data->viewQuery;
                $response = @mysqli_query($db_connect, $query);


            if($response) {

#               buildTable($page, $data->table, $data->allFields, $response);

                buildTable($page, $data->table, $data->viewFields, $response);
            }

	}


#RENDER ADD
	if($add) {
		echo '<div class="container">';
		buildAddForm($page, $data->allFields);
		echo '</div>';

	}


#RENDER EDIT
	if($edit) {
		echo '<div class="container">';
		buildEditForm($page, $data);
		echo '</div>';

	}

	if($viewStatus) {
		echo $statusDiv;

	}
?>

		</div>
	</div>
</body>
</html>
