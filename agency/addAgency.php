<?php
	include '../include/phpFunctions.php';
	include '../include/sqlData.php';
	
	$agency = new Agency;
?>



<?php 
			insert($agency->allFields, $agency->table);

			$submit_div = '';

			if(isset($_POST['countySubmit'])) {
                $data_missing = array();

                if(empty($_POST['countyID'])) {
                    $data_missing[] = 'countyID';

                } else {
                    $countyID = trim($_POST['countyID']);
                }

                if(empty($_POST['countyName'])) {
                    $data_missing[] = 'countyName';
                    echo 'missing';

                } else {
                    $countyName = trim($_POST['countyName']);
                }

  
				$note = trim($_POST['note']);



              if(empty($data_missing)) {

                    require_once('../../../mysqli/mysqli_connect.php');


                    $query = "INSERT INTO `county` (`CountyID`, `CountyName`, `Note`, `DateAdded`)  
                              VALUES(?,?,?,now())";


                    $stmt = $db_connect->prepare($query);

                    $stmt->bind_param('iss', $countyID, $countyName, $note);

                    if (!$stmt) {
                        die('mysqli error: '.mysqli_error($db_connect));
                    }


                    $affected_rows = mysqli_stmt_affected_rows($stmt);


                    if (!mysqli_execute($stmt)) {

						$submit_div = '<div class="alert alert-danger fade in">
    								   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    								   <strong>ERROR!</strong> ' . 
									   mysqli_stmt_error($stmt) . '.
  									   </div>';

                    } else {

						$query = "SELECT `DateAdded` 
								  FROM `county` 
								  WHERE `CountyID` = ". $countyID . "
								  LIMIT 1";

						$response = @mysqli_query($db_connect, $query);

						$dateAdded = mysqli_fetch_array($response)[0];

						$submit_div = '<div class="alert alert-success fade in">
    								   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    								   <strong>ADDED!</strong> ' . 
									   $countyName . ' County entered at ' . $dateAdded . '.
									   </div>';

						mysqli_close($db_connect);
						mysqli_stmt_close($stmt);
					}

				}

			}

?> 

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>County Add</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
	<?php
		buildNavBar()
	?>



<!--FORM START-->

	<div class="container">
		<?php 
			echo $submit_div;	
		?>


		<h2>Add New County</h2>
		<!-- <form action="countyAdded.php" method="post">i -->
		<form class="form-horizontal" role="form" action="" method="POST">
		
			<div class="form-group">
				<label class="control-label col-sm-2" for="countyID">
					*County ID:
				</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="countyID" size="10" value="" placeholder="Enter Unique Two Digit County ID"/>	
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="countyID">
					*County Name:
				</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="countyName" size="10" value="" placeholder="Enter Unique County Name"/>	
				</div>
			</div>
	
			<div class="form-group">	
				<label class="control-label col-sm-2" for="countyID">
					Add Notes:
				</label>
				<div class="col-sm-10">
					<textarea class="form-control" type="text" name="note" rows="4" size="10" value="" placeholder="Enter Note About This Add"></textarea>	
				</div>
			</div>
		
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">	
					<input class="btn btn-success" type="submit" name="countySubmit" value="Add County"/>
				</div>
			</div>

		</form>

<!--FORM END-->
	
	</div>

	</body>
</html>
