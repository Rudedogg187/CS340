<?php
	include '../include/phpFunctions.php';
	include '../include/sqlData.php';

	$county = new County;
?>



<?php 


			$submit_div = '';

			if(isset($_POST['countySubmit'])) {
                $data_missing = array();

                if(empty($_POST['countyID'])) {
                    $data_missing[] = 'countyID';

                } else {
                    $countyID = trim($_POST['countyID']);
                }

                if(empty($_POST['countyName'])) {
                    $data_missing[] = 'countyName';

                } else {
                    $countyName = trim($_POST['countyName']);
                }

  
				$note = trim($_POST['note']);



              if(empty($data_missing)) {

                    require_once('../../../mysqli/mysqli_connect.php');

					$query = insert($county->allFields, $county->table);
                    
					$stmt = $db_connect->prepare($query);

                    $stmt->bind_param('iss', $countyID, $countyName, $note);

                    if (!$stmt) {
                        die('mysqli error: '.mysqli_error($db_connect));
                    }


                    $affected_rows = mysqli_stmt_affected_rows($stmt);


                    if (!mysqli_execute($stmt)) {

						$submit_div = '<div class="alert alert-danger fade in">
    								   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    								   <strong>ERROR!</strong> ' . 
									   mysqli_stmt_error($stmt) . '.
  									   </div>';

                    } else {

						$query = "SELECT `DateAdded` 
								  FROM `county` 
								  WHERE `CountyID` = ". $countyID . "
								  LIMIT 1";

						$response = @mysqli_query($db_connect, $query);

						$dateAdded = mysqli_fetch_array($response)[0];

						$submit_div = '<div class="alert alert-success fade in">
    								   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    								   <strong>ADDED!</strong> ' . 
									   $countyName . ' County entered at ' . $dateAdded . '.
									   </div>';

						mysqli_close($db_connect);
						mysqli_stmt_close($stmt);
					}

				}

			}

?> 

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>County Add</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>

<!-- NAV BAR START -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/CS340/index.php">South West Seven Radio System</a>
            </div>

            <ul class="nav navbar-nav">
                <li class=""><a href="/CS340/index.php">Home</a></li>

                <li class="active dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Counties<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/CS340/county/viewCounties.php">View Counties</a></li>
                        <li><a href="/CS340/county/addCounty.php">Add County</a></li>
                        <li><a href="#">Delete County</a></li>
                        <li><a href="#">Edit County</a></li>
                    </ul>

                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Agencies<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/CS340/agency/viewAgencies.php">View Agencies</a></li>
                        <li><a href="/CS340/agency/addAgency.php">Add Agency</a></li>
                        <li><a href="#">Delete Agency</a></li>
                        <li><a href="#">Edit Agency</a></li>
                    </ul>

                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Radio Types<span class="caret"></span></a> <ul class="dropdown-menu">
                        <li><a href="#">View Radio Types</a></li>
                        <li><a href="#">Add Radio Type</a></li>
                        <li><a href="#">Delete Radio Type</a></li>
                        <li><a href="#">Edit Radio Type</a></li>
                    </ul>

                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Subscriber Radios<span class="caret"></span></a> <ul class="dropdown-menu">
                        <li><a href="#">View Radios</a></li>
                        <li><a href="#">Add Radio</a></li>
                        <li><a href="#">Delete Radio </a></li>
                        <li><a href="#">Edit Radio</a></li>
                    </ul>


        </div>
    </nav>

<!--NAV BAR END-->



<!--FORM START-->

	<div class="container">
		<?php 
			echo $submit_div;	
		?>


		<h2>Add New County</h2>
		<!-- <form action="countyAdded.php" method="post">i -->
		<form class="form-horizontal" role="form" action="" method="POST">
		
			<div class="form-group">
				<label class="control-label col-sm-2" for="countyID">
					*County ID:
				</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="countyID" size="10" value="" placeholder="Enter Unique Two Digit County ID"/>	
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="countyID">
					*County Name:
				</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="countyName" size="10" value="" placeholder="Enter Unique County Name"/>	
				</div>
			</div>
	
			<div class="form-group">	
				<label class="control-label col-sm-2" for="countyID">
					Add Notes:
				</label>
				<div class="col-sm-10">
					<textarea class="form-control" type="text" name="note" rows="4" size="10" value="" placeholder="Enter Note About This Add"></textarea>	
				</div>
			</div>
		
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">	
					<input class="btn btn-success" type="submit" name="countySubmit" value="Add County"/>
				</div>
			</div>

		</form>

<!--FORM END-->
	
	</div>

	</body>
</html>
