<html>
	<head>
		<title>Added</title>
	</head>
	<body>
		<?php
			if(isset($_POST['countySubmit'])) {
				$data_missing = array();

				if(empty($_POST['countyID'])) {
					$data_missing[] = 'countyID';
					echo 'missing';
				
				} else {
					$countyID = trim($_POST['countyID']);
				}

				if(empty($_POST['countyName'])) {
					$data_missing[] = 'countyName';
					echo 'missing';

				} else {
					$countyName = trim($_POST['countyName']);
					echo $countyName;
				}

				if(empty($data_missing)) {
		
					require_once('../../../mysqli/mysqli_connect.php');

			       	$query = "INSERT INTO `county` (`CountyID`, `CountyName`, `DateAdded`)  
							  VALUES(?,?,now())";


					$stmt = $db_connect->prepare($query);
					
					$stmt->bind_param('is', $countyID, $countyName);
				
					if (!$stmt) {
						die('mysqli error: '.mysqli_error($db_connect));
					}
					

					$affected_rows = mysqli_stmt_affected_rows($stmt);


					if (!mysqli_execute($stmt)) {
 						die('<br/>stmt error: '.mysqli_stmt_error($stmt));
					}



			#		mysqli_stmt_close($stmt);

				#	mysqli_close($db_connect);

				} else {

					echo 'The following data must be entered: <br/>';

					foreach($data_missing as $missing) {

						echo 'data';

					}
			
				}
			}	


		?>


	<div>
            <?php
                require_once('../../../mysqli/mysqli_connect.php');

                $query = 'SELECT `CountyID`, `CountyName`, `DateAdded` 
						  FROM `county`
						  ORDER BY `CountyName`';

                $response = @mysqli_query($db_connect, $query);

                if($response) {
                    echo
                        '<table> 
                            <tr>
                                <td><b>County ID</b></td>
                                <td><b>County Name</b></td>
                                <td><b>Date Added</b></td>
                                </tr>';

                    while($row = mysqli_fetch_array($response)){
                        echo
                            '<tr>
                                <td>' . $row['CountyID'] . '</td> 
                                <td>' . $row['CountyName'] . '</td>
                                <td>' . $row['DateAdded'] . '</td>
                            </tr>';

                    }
                }
		
				mysqli_stmt_close($stmt);

				mysqli_close($db_connect);

			
            ?>
        </div>
	</body>
</html>
