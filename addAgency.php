<html lang="en">

	<?php
		$page = 'Agency';

		include 'include/phpFunctions.php';
		include 'include/sqlData.php';

		$data = new Agency;

		buildHead($data->pageTitle);
	?>

<body>

	<?php
		buildNavBar($page);
	?>		

	<div class="container">
		<?php
			buildAddForm($page, $data->allFields);
		?>
	

	
	</div>

	</body>
</html>
