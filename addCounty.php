<html lang="en">

	<?php
		$page = 'County';

		include 'include/phpFunctions.php';
		include 'include/sqlData.php';

		$data = new County;

		buildHead($data->pageTitle);
	?>

<body>

	<?php
		buildNavBar($page);
	?>		

	<div class="container">
		<?php 
			
			buildAddForm($page, $data->allFields);

		?>
	
	</div>

	</body>
</html>
