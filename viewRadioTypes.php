<html lang="en">

	<?php
    	$page = 'RadioType';

		include 'include/phpFunctions.php';
		include 'include/sqlData.php';

 	   	$data = new RadioType;

        buildHead($data->pageTitle);
	?>

<?php
	$submit_div = '';

		if(isset($_POST['delete' . $page  ])) {

				$where = $_POST['where'];
				$dbTable = $_POST['dbTable'];

				$query = "DELETE FROM " . $dbTable . ' ' . $where;

				require_once('../../mysqli/mysqli_connect.php');

				$response = @mysqli_query($db_connect, $query);

 				$submit_div = '<div class="alert alert-success fade in">
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                       <strong>Success:</strong>  ' . $query . '</div>';

			}

		?>

    <?php

    ?>

<body>
	<?php
    	buildNavBar($page);
	?>

    <div class="container">
        <?php
            echo $submit_div;
				require_once('../../mysqli/mysqli_connect.php');
   

    			$query = selectAll($data->allFields, $data->table);

     		 	$response = @mysqli_query($db_connect, $query);

 
       		if($response) {

				buildTable($page, $data->table, $data->allFields, $response);
			}
        ?>


	</div>
</body>
</html>
