<style>
	.btn-fixed {
		width: 100% !important;
	}
</style>



<?php

	include 'sqlData.php';	


	function selectAll($fields, $table) {
		$selectAll = 'SELECT ';
        foreach ($fields as $field) {
       		$selectAll = $selectAll . $field[0] . ', ';    
        }       
        
        $selectAll = substr($selectAll, 0, -2); 
        $selectAll = $selectAll . ' FROM ' . $table;

        return $selectAll;
	}


	function insert($fields, $table) {
		$insert = 'INSERT INTO ' . $table . '(';
		$v = '';

		foreach ($fields as $field) {
			$insert = $insert . $field[0] . ', ';
			
			if ($field[0] == '`DateAdded`' || $field[0] == '`LastModified`') {
				$v = $v . 'now(), ';
			} else {
				$v = $v . '?, ';
			}

		}	
        
		$insert = substr($insert, 0, -2); 
        $v = substr($v, 0, -2); 

		$insert = $insert . ') VALUES('. $v . ')';
		return $insert;	
		
	}


	function buildEditForm($page, $data) {
		$fields = $data->allFields;
		$table = $data->table;
		echo '<h2>Edit ' . $page . '</h2>';
		echo '<form class="form-horizontal" role="form" action="index.php" method="POST">';

		$first = True;
		foreach($fields as $field) {
			if($field[4] > 0 || $field[4] == -1) {
				echo '<div class="form-group">';
				echo '<label class="control-label col-sm-2" for="' . $field[0] . '">';
				if($field[4] == 1) {
					echo '*';
				}	
				if($field[4] == -1) {
					echo  '*' . $page . ' to Edit:';	
				} else {
					echo 'New ' . $field[1] . ':';
				}

				echo '</label>';
				echo '<div class="col-sm-10">';

					if($first) { #$field[4] == -1 || $field[0] == "`CountyID`) {
						$first = False;
						require_once('../../mysqli/mysqli_connect.php');

						$query = "SELECT ". $fields[1][0] . ", " . $fields[0][0] . " from " . $table;
						$response = @mysqli_query($db_connect, $query);
								
							echo '<select class="form-control" name="' . $field[0] . '">';

							while ($row = mysqli_fetch_array($response)) {
								$name = $row[0];
								$id = $row[1];
								echo '<option value="' . $id . '">' . $id . ' (' . $name . ')</option>';
							}
								
							echo '</select>';

					} elseif($field[3] == 'f') {

						require_once('../../mysqli/mysqli_connect.php');

						$query = $field[5];
						$response = @mysqli_query($db_connect, $query);
								
							echo '<select class="form-control" name="' . $field[0] . '">';

							while ($row = mysqli_fetch_array($response)) {
								$name = $row[0];
								$id = $row[1];
								echo '<option value="' . $id . '">' . $id . ' (' . $name . ')</option>';
							}
								
							echo '</select>';
				
					} else {

						if($field[4] == 4) {
							echo '<textarea rows="4" ';	
						} else {
							echo '<input ';
						}

						
						echo ' class="form-control" type="text" name="' . $field[0] . '" size="10" value=""';

						if($field[4] == 4) {
							echo '></textarea>';
						} else {
							echo '/>';
						}
					}
					echo '</div>';
					echo '</div>';
			}
		}


		echo '<div class="form-group">';
		echo '<div class="col-sm-10 col-sm-offset-2">';
		echo '<input class="btn btn-success" type="submit" name="edit' . $page . '" value="Edit '. $page . '"/>';
		echo '</div>';
		echo '</div>';
		echo '</form>';



	}



	function buildAddForm($page, $fields) {
		echo '<h2>Add New ' . $page . '</h2>';
		echo '<form class="form-horizontal" role="form" action="index.php" method="POST">';

		foreach($fields as $field) {
			if($field[4] > 0) {
				echo '<div class="form-group">';
				echo '<label class="control-label col-sm-2" for="' . $field[0] . '">';
				if($field[4] == 1) {
					echo '*';
				}	
				echo $field[1] . ':';
				echo '</label>';
				echo '<div class="col-sm-10">';



				if($field[3] == 'f') {

				    require_once('../../mysqli/mysqli_connect.php');

                	$query = $field[5];
                	$response = @mysqli_query($db_connect, $query);
					#if($response) {
						
						echo '<select class="form-control" name="' . $field[0] . '">';

						while ($row = mysqli_fetch_array($response)) {
							$name = $row[0];
							$id = $row[1];
							echo '<option value="' . $id . '">' . $id . ' (' . $name . ')</option>';
						}
				      	
						echo '</select>';
				#	}

				} else {

					if($field[4] == 4) {
						echo '<textarea rows="4" ';	
					} else {
						echo '<input ';
					}

				
					echo ' class="form-control" type="text" name="' . $field[0] . '" size="10" value=""';

					if($field[4] == 4) {
						echo '></textarea>';
					} else {
						echo '/>';
					}
				}
				echo '</div>';
				echo '</div>';
			}
		}

		echo '<div class="form-group">';
		echo '<div class="col-sm-10 col-sm-offset-2">';
		echo '<input class="btn btn-success" type="submit" name="submit' . $page . '" value="Add '. $page . '"/>';
		echo '</div>';
		echo '</div>';
		echo '</form>';

	}


	function buildTable($page, $dbTable, $fields, $response) {
		$table = '<table class="table table-hover">
				  <tr>';
	
		foreach ($fields as $field) {
			$table = $table . '<td><b>' . $field[1] . '</b></td>';
		}

		
	
		$table = $table . '
				<td></td>
				<td></td>
			</tr>';
	
		
		while ($row = mysqli_fetch_array($response)) {
			$table = $table . '<tr>';

			$where = 'WHERE ';
			$i=0;
			foreach ($fields as $field) {
		
				if ($field[3] == 'p') {	
					if ($field[2] == 's') {
						$data = "'" . $row[$i] . "'";
		
					} else {
						$data = $row[$i];
					}

					$where = $where . $field[0] . ' = ' . $data . ' AND ';
				}

				$table = $table . '<td>' . $row[$i] . '</td>';
				$i++;
			}	

			$where = substr($where, 0, -5);
	
			$table = $table . '
					<td>	
						<button type="button" class="btn btn-info btn-xs">Edit</button>
					</td>
					<td>	
						<form action="index.php" method="POST">
							<input type="hidden" name="where" value=" '.  $where . '">	
							<input type="hidden" name="dbTable" value=" '. $dbTable .'">
							<input type="submit" class="btn btn-danger btn-xs" name="delete' .$page . '" value="Delete"/>
						</form>
					</td>
				</tr>';
		}
		
		$table = $table . '</table>';

		echo $table;
	}


	function formatData($data, $type) {

		if ($type == 's') {
			$data = "'" . $data . "'";
		}
	
		return $data;
	}




	function buildHead($pageTitle) {
		echo '<head>
    			<meta charset="UTF-8" />
    			<title>' . $pageTitle . '</title>
    			<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    			<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
			<script src="js/script.js"></script>	
		     </head>';
	}


	function buildNavBar($dataLst) { 
		echo
			'<nav class="navbar navbar-inverse">
        		<div class="container-fluid">
            		<div class="navbar-header">
                		<a class="navbar-brand" href="https://www.radioreference.com/apps/db/?sid=5641">South West Seven Radio System</a>
            		</div>

            		<ul class="nav navbar-nav">
                		<li class=""><a href="docs/hughesDb.pdf">DB Design</a></li>';

		$pg = 'County';	

		foreach($dataLst as $page => $data) {
			echo '<li class="';
			
			if($page == $pg) {
				echo 'active';
			}

		echo 'dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">' . $data->pageTitle . 
					'<span class="caret"></span></a>
                    	<ul class="dropdown-menu">
							<form action="index.php" method="POST">
								<li><input class="btn btn-default btn-sm btn-fixed" type="submit" name="viewRender' . $page . '" value="Veiw ' . $page . '"></li> 

                       			<li><input class="btn btn-default btn-sm btn-fixed" type="submit" name="addRender' . $page . '" value="Add ' . $page . '"></li> 

                       			<li><input class="btn btn-default btn-sm btn-fixed"type="submit" name="editRender' . $page . '" value="Edit ' . $page . '"></li> 
							</form>
                    	</ul>';


		}
		echo '</ul>';
		echo '<ul class="nav navbar-nav navbar-right">
					<li><a href="https://gitlab.com/Rudedogg187/CS340.git">Git Lab Repo</a></li>
				</span>
			</ul>';
  		echo '</div> </nav>';
	}

?>
