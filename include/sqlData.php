<?php

    $dataLst = [
        'County'=> new County,
        'Agency' => new Agency,
        'RadioType' => new RadioType,
        'Radio' => new Radio,
        'StatusType' => new StatusType,
        'RadioStatus' => new RadioStatus
    ];


	class County {
		var $table = '`county`';
		var $allFields = [
			['`CountyID`', 		'County ID', 		'i',	'p', 	1],
			['`CountyName`', 	'County Name', 		's',	'',		1],
			['`DateAdded`', 	'Date Added', 		's',	'',		0],
			['`LastModified`', 	'Last Modified', 	's',	'',		0],
			['`Note`', 			'Notes', 			's',	'',		4]
		];
		var $pageTitle = 'Counties';
		var $viewFields = [
			['`CountyID`',		'County ID',		'i',	'p'],
			['`CountyName`',	'County Name',		's',	''],
			['`AgencyCount`', 	'Agency Count',		's',	''],
			['`RadioCount`',	'Radio Count', 		'i',	''],
			['`DateAdded`', 	'Date Added', 		's',	''],
			['`LastModified`',	'Modified',			's',	''],
			['`Note`',			'Notes',			's',	'']
		];
		var $viewQuery = "SELECT c.CountyID, c.CountyName, CAST(COUNT(DISTINCT a.AgencyID) AS UNSIGNED) AgencyCount, CAST(COUNT(DISTINCT r.RadioID) AS UNSIGNED) RadioCount, c.DateAdded, c.LastModified, c.Note
						FROM county c
						LEFT JOIN agency a
						ON a.CountyID = c.CountyID
						LEFT JOIN radio r
						ON r.AgencyID = a.AgencyID
						LEFT JOIN radioType rt
						ON r.radioTypeID = rt.radioTypeID
						GROUP BY c.CountyID, c.CountyName, c.DateAdded, c.LastModified, c.Note";


	}

	class Agency {
		var $table = '`agency`';
		var $allFields = [
			['`AgencyID`', 		'Agency ID', 	'i',	'p',	-1,	""],
			['`AgencyAbbr`', 	'Agency Short', 's',	'',		1,	""],   
			['`AgencyName`', 	'Agency Name', 	's',	'',		1,	""],
			['`CountyID`', 		'County ID', 	'i',	'f',	1, 	"SELECT CountyName, CountyID FROM county"], 
			['`DateAdded`', 	'Date Added', 	's',	'',		0,	""],
			['`LastModified`', 	'Last Modified','s',	'',		0,	""],
			['`Note`', 			'Notes', 		's',	'',		4,	""]
		];
		var $pageTitle = 'Agencies';
		var $viewFields = [
			['`AgencyID`',		'Agency ID',	'i',	'p'],
			['`AgencyName`',	'Agency Name',	's',	''],
			['`CountyName`', 	'County Name',	's',	''],
			['`RadioCount`',	'Radio Count', 	'i',	''],
			['`DateAdded`', 	'Date Added', 	's',	''],
			['`LastModified`',	'Modified',		's',	''],
			['`Note`',			'Notes',		's',	'']
		];
		var $viewQuery ="SELECT a.AgencyID, CONCAT(a.AgencyName, ' (', a.AgencyAbbr, ')') AgencyName, c.CountyName, CAST(COUNT(DISTINCT r.RadioID) AS UNSIGNED) RadioCount, a.DateAdded, a.LastModified, a.Note
						FROM agency a
						LEFT JOIN radio r	
						ON r.AgencyID = a.AgencyID
						LEFT JOIN radioType rt
						ON r.radioTypeID = rt.radioTypeID
						LEFT JOIN county c
						ON a.CountyID = c.CountyID
						GROUP BY a.AgencyID, a.AgencyName, a.AgencyAbbr, c.CountyName, a.DateAdded, a.LastModified, a.Note";

	}

	class RadioType {
		var $table = '`radioType`';
		var $allFields = [
			['`RadioTypeID`', 	'Type ID', 			'i',		'p', 	-1],
		    ['`RadioTypeAbbr`', 'Type Short',		's',		'',		1], 
   			['`RadioTypeName`', 'Type Name', 		's',		'',		1],
			['`DateAdded`',		'Date Added', 		's',		'',		0], 
  			['`LastModified`',  'Last Modified',	's',		'',		0],
 		  	['`Note`',			'Notes', 			's',		'',		4]

		];
		var $pageTitle = 'Radio Types';
		var $viewFields = [
			['`RadioTypeID`',		'Radio Type ID',	'i',	'p'],
			['`RadioType`',			'Radio Type',		's',	''],
			['`RadioCount`',		'Radio Count', 		'i',	''],
			['`DateAdded`', 		'Date Added', 		's',	''],
			['`LastModified`',		'Modified',			's',	''],
			['`Note`',				'Notes',			's',	'']

		];
		var $viewQuery = "SELECT rt.RadioTypeID, CONCAT(rt.RadioTypeName, ' (', rt.RadioTypeAbbr, ')') RadioType, CAST(COUNT(DISTINCT r.RadioID) AS UNSIGNED) RadioCount, rt.DateAdded, rt.LastModified, rt.Note
						FROM radioType rt
						LEFT JOIN radio r
						ON rt.RadioTypeID = r.RadioTypeID
						GROUP BY rt.RadioTypeID, CONCAT(rt.RadioTypeName, ' (', rt.RadioTypeAbbr, ')'),  rt.DateAdded, rt.LastModified, rt.Note";

	}	
	
	class Radio {
		var $table = '`radio`';
		var $allFields = [
			['`RadioID`',	 	'Radio ID',			'i',		'p', 	1],
		    ['`RadioSN`', 		'Radio SN',			's',		'',		1], 
   			['`RadioUserAlias`', 'Radio Alias',		's',		'',		1],
			['`AgencyID`',		'Agency ID',		'i',		'f',	1,	"SELECT AgencyAbbr, AgencyID FROM agency"],
			['`RadioTypeID`',	'Type ID',			'i',		'f',	1, 	"SELECT RadioTypeName, RadioTypeID from radioType"],
			['`DateAdded`',		'Date Added', 		's',		'',		0], 
  			['`LastModified`',  'Last Modified',	's',		'',		0],
 		  	['`Note`',			'Notes', 			's',		'',		4]

		];
		var $pageTitle = 'Radios';
		var $viewFields = [
			['`RadioID`',			'Radio ID',		'i',	'p'],
			['`RadioSN`',			'Radio SN', 	'i',	''],
			['`RadioUserAlias`',	'Radio Alias',	's',	''],
			['`RadioType`',			'Radio Type',	's',	''],
			['`Agency`',			'Agency',		's',	''],
			['`CountyName`', 		'County Name',	's',	''],
			['`DateAdded`', 		'Date Added', 	's',	''],
			['`LastModified`',		'Modified',		's',	''],
			['`Note`',				'Notes',		's',	'']

		];
		var $viewQuery = "SELECT r.RadioID, r.RadioSN, r.RadioUserAlias, rt.RadioTypeName, a.AgencyAbbr Agency, c.CountyName, r.DateAdded, r.LastModified, r.Note
						FROM radio r
						LEFT JOIN radioType rt
						ON r.RadioTypeID = rt.RadioTypeID
						LEFT JOIN agency a
						ON r.AgencyID = a.AgencyID
						LEFT JOIN county c 
						ON a.CountyID = c.CountyID";

	}	

	class StatusType {
		var $table = '`statusType`';
		var $allFields = [
			['`StatusTypeID`', 	'Status ID', 		'i',		'p', 	-1],
		    ['`StatusTypeAbbr`', 'Status Short',	's',		'',		1], 
   			['`StatusTypeName`', 'Status Name',		's',		'',		1],
			['`DateAdded`',		'Date Added', 		's',		'',		0], 
  			['`LastModified`',  'Last Modified',	's',		'',		0],
 		  	['`Note`',			'Notes', 			's',		'',		4]

		];
		var $pageTitle = 'Status Types';
		var $viewFields = [
			['`StatusTypeID`', 	'Status ID', 		'i',		'p'],
		    ['`StatusTypeAbbr`', 'Status Short',	's',		''], 
   			['`StatusTypeName`', 'Status Name', 	's',		''],
			['`DateAdded`',		'Date Added', 		's',		''], 
  			['`LastModified`',  'Last Modified',	's',		''],
 		  	['`Note`',			'Notes', 			's',		'']
		];
		var $viewQuery = "SELECT * from statusType";
	}

	class RadioStatus {
		var $table = '`radioStatus`';
		var $allFields = [
			['`RadioStatusID`', 	'Radio Status ID',	'i',		'p', 	-1],
		    ['`RadioID`', 			'Radio ID',			's',		'f',	1, 	"SELECT RadioUserAlias, RadioID FROM radio ORDER BY RadioUserAlias"], 
   			['`StatusTypeID`', 		'Status ID', 		's',		'f',	1,	"SELECT StatusTypeName, StatusTypeID FROM statusType ORDER BY StatusTypeName"],
			['`DateAdded`',			'Date Added', 		's',		'',		0], 
 		  	['`Note`',				'Notes', 			's',		'',		4]
		];
		var $pageTitle = 'Status Types';
		var $viewFields = [
			['`RadioStatusID`',		'Radio Status ID',	'i',		'p'],
			['`AgencyAbbr`', 		'Agency', 			's',		''],
		    ['`RadioTypeName`',		'Radio Type',		's',		''], 
   			['`RadioID`', 			'Radio ID', 		'i',		''],
			['`RadioSN`',			'Radio SN', 		's',		''], 
 		  	['`RadioUserAlias`',	'Radio Alias',		's',		''],
   			['`StatusTypeName`', 	'Status ID', 		's',		''],
			['`DateAdded`',			'Date Added', 		's',		''], 
			['`Note`',				'Date Added', 		's',		''], 

		];
		var $viewQuery = "SELECT RadioStatusID, a.AgencyAbbr, rt.RadioTypeName, r.RadioID, r.RadioSN, r.RadioUserAlias, st.StatusTypeName, rs.DateAdded, rs.Note
					FROM radioStatus rs
					LEFT JOIN radio r
					on rs.RadioID = r.RadioID
					LEFT JOIN statusType st
					on rs.StatusTypeID = st.StatusTypeID
					LEFT JOIN agency a
					on r.AgencyID = a.AgencyID
					LEFT JOIN radioType rt
					on r.RadioTypeID = rt.RadioTypeID";
	}


	class Admin {
		var $drop = [
			"DROP TABLE IF EXISTS `radioStatus`",
			"DROP TABLE IF EXISTS `statusType`",
			"DROP TABLE IF EXISTS `radio`",
			"DROP TABLE IF EXISTS `radioType`",
			"DROP TABLE IF EXISTS `agency`",
			"DROP TABLE IF EXISTS `county`"
		];
		var $create = [
				"CREATE TABLE IF NOT EXISTS `county`
				(
					`CountyID` INT NOT NULL AUTO_INCREMENT,
					`CountyName` VARCHAR(50) NOT NULL,
					`DateAdded` DATETIME NOT NULL,
					`LastModified` DATETIME NOT NULL,
					`Note`  VARCHAR(10000),
					PRIMARY KEY(`CountyID`),
					UNIQUE KEY(`CountyName`)
				) ENGINE=INNODB",
				"CREATE TABLE IF NOT EXISTS `agency`
				(
					`AgencyID` INT NOT NULL AUTO_INCREMENT,
					`AgencyAbbr` VARCHAR(10) NOT NULL,
					`AgencyName` VARCHAR(50) NOT NULL,
					`CountyID` INT NOT NULL,
					`DateAdded` DATETIME NOT NULL,
					`LastModified` DATETIME NOT NULL,
					`Note` VARCHAR(10000),
					PRIMARY KEY (`AgencyID`),
					FOREIGN KEY (`CountyID`) REFERENCES `county`(`CountyID`)
						ON DELETE CASCADE
						ON UPDATE CASCADE,
					UNIQUE KEY(`AgencyAbbr`),
					UNIQUE KEY(`AgencyName`)
				) ENGINE=INNODB",
				"CREATE TABLE IF NOT EXISTS `radioType`
				(
					`RadioTypeID` INT NOT NULL AUTO_INCREMENT,
					`RadioTypeAbbr` VARCHAR(10) NOT NULL,
					`RadioTypeName` VARCHAR(50) NOT NULL,
					`DateAdded` DATETIME NOT NULL,
					`LastModified` DATETIME NOT NULL,
					`Note` VARCHAR(10000),
					PRIMARY KEY (`RadioTypeID`),
					UNIQUE KEY(`RadioTypeAbbr`),
					UNIQUE KEY(`RadioTypeName`)
				) ENGINE=INNODB",
				"CREATE TABLE IF NOT EXISTS `radio` 
				(
					`RadioID` INT NOT NULL,
					`RadioSN` VARCHAR(30),
					`RadioUserAlias` VARCHAR(30),
					`AgencyID` INT NOT NULL,
					`RadioTypeID` INT NOT NULL,
					`DateAdded` DATETIME NOT NULL,
					`LastModified` DATETIME NOT NULL,
					`Note` VARCHAR(10000),
					PRIMARY KEY (`RadioID`),
					FOREIGN KEY (`AgencyID`) REFERENCES `agency`(`AgencyID`)
						ON DELETE CASCADE
						ON UPDATE CASCADE,
					FOREIGN KEY (`RadioTypeID`) REFERENCES `radioType`(`RadioTypeID`)
						ON DELETE CASCADE
						ON UPDATE CASCADE,
					UNIQUE KEY(`RadioSN`),
					UNIQUE KEY(`RadioUserAlias`)
				) ENGINE=INNODB",
				"CREATE TABLE IF NOT EXISTS `statusType`
				(
					`StatusTypeID` INT NOT NULL AUTO_INCREMENT,
					`StatusTypeAbbr` VARCHAR(10) NOT NULL,
					`StatusTypeName` VARCHAR(50) NOT NULL, 
					`DateAdded` DATETIME NOT NULL,
					`LastModified` DATETIME NOT NULL,
					`Note` VARCHAR(10000),
					PRIMARY KEY (`StatusTypeID`),
					UNIQUE KEY(`StatusTypeAbbr`),
					UNIQUE KEY(`StatusTypeName`)
				) ENGINE=INNODB",
				"CREATE TABLE IF NOT EXISTS `radioStatus`
				(
					`RadioStatusID` INT NOT NULL AUTO_INCREMENT,
					`RadioID` INT NOT NULL,
					`StatusTypeID` INT NOT NULL,
					`DateAdded` DATETIME NOT NULL,
					`Note` VARCHAR(10000),
					PRIMARY KEY (`RadioStatusID`),
					FOREIGN KEY (`RadioID`) REFERENCES `radio`(`RadioID`)
						ON DELETE CASCADE
						ON UPDATE CASCADE,
					FOREIGN KEY(`StatusTypeID`) REFERENCES `statusType`(`StatusTypeID`)
						ON DELETE CASCADE
						ON UPDATE CASCADE
				) ENGINE=INNODB"
		];
	}
?>
